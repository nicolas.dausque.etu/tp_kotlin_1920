package com.tpKotlin.glhf.tpkotlin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tpKotlin.glhf.tpkotlin.entity.FavDir;
import com.tpKotlin.glhf.tpkotlin.entity.Meme;
import com.tpKotlin.glhf.tpkotlin.service.DossierFavService;

@Controller
public class SimpleController {

	@Autowired
	private DossierFavService dfs;

	@RequestMapping({ "/", "/index" })
	public ModelAndView index() {
		return new ModelAndView("index", "msg", "Bienvenue !");
	}

	@RequestMapping("/ex1")
	public ModelAndView exo1() {
		String s = FavDir.affiche();
		ModelAndView mav = new ModelAndView("ex1");
		mav.addObject("dirfav", s );
		return mav;
	}
	
	@RequestMapping("/ex2")
	public ModelAndView exo2() {
		return new ModelAndView("ex2", "formMeme", new Meme("", ""));
	}

	@RequestMapping("/ex3")
	public ModelAndView exo3() {
		return new ModelAndView("ex3");
	}

	@RequestMapping("/ex4")
	public ModelAndView exo4() {
		String s = dfs.getAllImagesFromFavFASTER();
		ModelAndView mav = new ModelAndView("ex4");
		mav.addObject("dirfav", s );
		return mav;
	}
	
	@RequestMapping("/ex5")
	public ModelAndView exo5() {
		return new ModelAndView("ex5");
	}
}
