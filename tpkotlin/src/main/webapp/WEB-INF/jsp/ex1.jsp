<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:import url="/WEB-INF/jsp/header.jsp" />
<title>Etape 1</title>
</head>
<body>
	<h1 class="content-title">Etape 1 : Singleton</h1>
	<br>
	<br>
	<div class="row">
		<div class="offset-sm-1 col-sm-10">
			<h2>Mon Repo Favoris :</h2>
			<br>
			<br>
			<ul>
				<c:forTokens items="${dirfav}" delims="&&&" var="name">
					 <li> ${name} </li>
				</c:forTokens>
			</ul>
		</div>
	</div>
</body>
</html>