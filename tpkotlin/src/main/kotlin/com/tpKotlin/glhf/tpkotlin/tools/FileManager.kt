package com.tpKotlin.glhf.tpkotlin.tools

import java.io.File
import com.tpKotlin.glhf.tpkotlin.entity.FavDir
import com.tpKotlin.glhf.tpkotlin.entity.Meme
import java.io.FileOutputStream

object FileManager {
	val FILE_NAME = "monTonton.sappellePasRichard"
	val PATH_TO_FILE = "src/main/resources/files/"

	fun thereIsSomethingInTheFile(): Boolean {
		var res = false
		File(PATH_TO_FILE + FILE_NAME).bufferedReader().lines().use { lines ->
			for (line in lines) {
				if (!line.startsWith('#')) {
					res = true
					break
				}
			}
		}

		return res
	}

	fun loadAllFromFile() {
		lateinit var tab: List<String>
		var nom: String = ""

		File(PATH_TO_FILE + FILE_NAME).forEachLine {
			if (!it.startsWith('#')) {
				tab = it.split(" ")
				if (tab.size == 2) {
					FavDir.loadMeme(Meme(tab[0], tab[1]))
				} else if(tab.size > 2) {
					for(i in 0 until tab.size-1) {
						nom += tab[i] + " "
					}
					FavDir.loadMeme(Meme(nom, tab[tab.size-1]))
				}
			}
		}
	}

	fun saveMemeInFile(name: String, url: String) {
		FileOutputStream(File(PATH_TO_FILE + FILE_NAME), true)
			.bufferedWriter()
			.use {
				it.newLine()
				it.write(name.trim() + " " + url.trim())
			}
	}
}