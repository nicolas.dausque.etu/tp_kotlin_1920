package com.tpKotlin.glhf.tpkotlin.service

import com.tpKotlin.glhf.tpkotlin.dao.ImageAccessorDao

import kotlinx.coroutines.*
import com.tpKotlin.glhf.tpkotlin.entity.FavDir
import com.tpKotlin.glhf.tpkotlin.entity.Meme
import org.springframework.stereotype.Service
import com.tpKotlin.glhf.tpkotlin.dao.ImageAccessorDaoImpl
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDateTime

@Service
class DossierFavServiceImpl : DossierFavService {

	@Autowired
	lateinit var imgDao: ImageAccessorDao

	//méthode check + ajout exo 2
	override fun insertMeme(meme: Meme) {
		FavDir.addMeme(meme)
	}

	//Truc lent pour l'exo 3
	override fun getAllImagesFromFav(): String {
		var liste = FavDir.getAll()
		var s: String = ""
		var res: Boolean = false
		for (i in 0 until liste.size) {
			s = liste.get(i).url + "&&&" + s
		}
		for (img in liste) {
			println("> here we go")
			res = imgDao.getMeme(img.nom)
		}

		return if (res) s else "fail"
	}

	//Truc rapide pour le 4
	override fun getAllImagesFromFavFASTER(): String {
		var liste = FavDir.getAll()
		var jobList: MutableList<Job> = mutableListOf()
		var s: String = ""

		for (i in 0 until liste.size) {
			s = liste.get(i).url + "&&&" + s
		}

		runBlocking {
			for (img in liste) {
				println("> service lance l'appel DAO")
				jobList.add(GlobalScope.launch { imgDao.getMeme(img.nom) })
			}

			for (myJob in jobList) {
				myJob.join()
				println("" + LocalDateTime.now() + " >> affichage ? " + myJob.isActive)
			}
		}
		return s
	}
}