package com.tpKotlin.glhf.tpkotlin.entity

import java.time.LocalDateTime
import com.tpKotlin.glhf.tpkotlin.tools.FileManager

object FavDir {
	val list: MutableList<Meme> = mutableListOf()

	init {
		println("Dossier de favoris crée")
		if (FileManager.thereIsSomethingInTheFile()) {
			FileManager.loadAllFromFile()
		}
	}

	fun addMeme(meme: Meme) {
		list.add(meme)
		FileManager.saveMemeInFile(meme.nom, meme.url)
	}
	
	fun loadMeme(meme: Meme) {
		list.add(meme)
	}

	@JvmStatic
	fun affiche(): String {
		var s: String = ""
		for (i in 0 until list.size) {
			s = list.get(i).nom + " " + list.get(i).url + "&&&"+ s
		}
		return s
	}

	fun getAll(): MutableList<Meme> {
		return list
	}
}

data class Meme(var nom: String, var url: String)


