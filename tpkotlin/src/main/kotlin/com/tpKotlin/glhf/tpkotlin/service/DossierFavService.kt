package com.tpKotlin.glhf.tpkotlin.service

import com.tpKotlin.glhf.tpkotlin.entity.Meme

interface DossierFavService {
	fun insertMeme(meme: Meme)
	fun getAllImagesFromFav(): String
	fun getAllImagesFromFavFASTER(): String
}