package com.tpKotlin.glhf.tpkotlin.controller

import com.tpKotlin.glhf.tpkotlin.entity.Meme
import com.tpKotlin.glhf.tpkotlin.service.DossierFavService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

@Controller
@RequestMapping("meme")
class MemeController {
	
	@Autowired
	lateinit var favService: DossierFavService
	
	@PostMapping("/add")
	fun addMeme(@ModelAttribute("formMeme") formMeme: Meme): ModelAndView {
		favService.insertMeme(formMeme)
		
		var mav = ModelAndView("ex2", "succeed", "true")
		mav.addObject("formMeme", Meme("", ""))
		return mav
	}
}