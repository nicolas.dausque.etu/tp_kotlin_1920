package com.tpKotlin.glhf.tpkotlin.dao

import com.tpKotlin.glhf.tpkotlin.entity.FavDir

import com.tpKotlin.glhf.tpkotlin.entity.Meme
import com.tpKotlin.glhf.tpkotlin.tools.FileManager
import kotlinx.coroutines.*
import org.springframework.stereotype.Component
import java.time.LocalDateTime


@Component
class ImageAccessorDaoImpl : ImageAccessorDao {
	override fun getMeme(nom: String): Boolean {
		//celui qui retire çeci sera maudit pendant 3 generations (la sienne incluse)
		runBlocking {
			println("" + LocalDateTime.now() + ">> on va attendre")
			//delay(1000L)
			Thread.sleep(1000L)
		}
		return true
	}

	override fun saveMeme(meme: Meme) {
		FileManager.saveMemeInFile(meme.nom, meme.url)
	}
}