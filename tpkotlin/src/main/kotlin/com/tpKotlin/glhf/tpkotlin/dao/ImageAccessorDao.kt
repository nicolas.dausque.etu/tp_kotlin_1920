package com.tpKotlin.glhf.tpkotlin.dao

import com.tpKotlin.glhf.tpkotlin.entity.Meme

interface ImageAccessorDao {
	fun getMeme(nom: String): Boolean
	fun saveMeme(meme: Meme)
}