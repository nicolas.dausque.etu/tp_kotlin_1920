# TP Kotlin
---
**PS :** Vous pouvez skip l'intro et aller à __l'Étape 1__ si vous n'aimez pas les thrillers de science-fiction autobiographiques.

### Précédemment, dans _la vie_
Il était une fois une team de développeurs aguérris, forts d'une expérience quasi-infinie (i.e. 5 ans et demi) dans le domaine des applications web (chacuns ses délires, on ne juge pas).

Un beau jour alors que la migration de l'appli vers Java 12 battait son plein et que le pôle de recherche squattait le baby depuis 40 minutes, Jaquot Tlin, jeune dév en devenir, dingue de Dave, eut une idée. Une idée qu'aucun de ses compères développeurs ne pouvait appréhender :  
&nbsp;&nbsp;\- _"Eh les gars, et si au lieu de faire encore et toujours du Java (qui a presque 25 ans), on gardait ce qu'on avait là et on regardait comment faire du Kotlin ? On a quasiment rien à changer et ça pourrait nous simplifier notre vie de développeurs pas toujours facile !"_  
&nbsp;&nbsp;\- _\*silence dans l'open space\*_

Prise de stupeur et de tremblements, la dév sénior du pôle, Eve D. Lopper, attrapa Jaquot d'un regard, l'invitant silencieusement à _slide_ à son bureau avec sa chaise.  
&nbsp;&nbsp;\- _"OK Jaquot écoute moi bien. J'aime les gens qui prennent des initatives. Mais j'ai pas le temps de suivre des tutos youtube sur les nouvelles techs. Tu peux me montrer en deux-deux ce qu'est Kotlin ?"_

&nbsp;&nbsp;\- _"Ça tombe bien j'ai justement un vieux TP d'IAGL sous la main, pousse toi, et admire..."_
![Alt Text](https://media.giphy.com/media/Z46IdUzMRYqyI/giphy.gif)

## Étape 1 : Du Java et des Hommes
Premièrement, ~~on va~~ vous allez faire du Java, parce que le Java, c'est connu.  
Rien de bien compliqué pour commencer, juste un singleton [comme il ne faut plus en faire](https://en.wikipedia.org/wiki/Singleton_pattern) (<- c'est un lien, il [peut](https://www.conjugaisonfrancaise.com/pouvoir.html) y en avoir des utiles).

Vous vous en doutez, vu que c'est du Java et pas du Kotlin comme annoncé, vous pouvez le faire un peu où vous voulez. **MAIS** sachez que vous en aurez quand même besoin dans un futur très proche, donc on vous conseille de le faire sur un compilateur en ligne ou un projet bidon, histoire de voir si ça compile.

##### TODO (vous pouvez les cocher même si ça ne sert à rien) :  
- [ ] Créer une classe `Meme` qui représente un [meme](https://en.wikipedia.org/wiki/Meme) et qui comporte un nom _nom_ et un url _url_ (ouais on fait dans la sobriété)
- [ ] Créer une classe `FavDir` **singleton** en _JAVA_ qui représente un dossier de favoris tout simple. Il contiendra une liste d'objets _Meme_.

Si vous voulez des images mais que vous n'avez pas d'idées (franchement [Google images](https://duckduckgo.com/) c'est pas compliqué), [rendez-vous ici](https://knowyourmeme.com/memes/popular). Tant que c'est une image, ça va (jpg, png, gif...)

## Étape 1,5 : Transformation Kotlin
Parfait, maintenant que le singleton est derrière vous, vous allez pouvoir l'oublier et le refaire. Cette fois-ci bien sûr, en Kotlin, tout beau tout propre, donc ne l'oubliez pas trop vite non plus.  
Pensez à jeter un œil aux slides, on vous en a parlé dedans.  
**PRO TIPS** : _object_, _data class_

Alternativement, les plus chanceux d'entre vous qui utilisent IntelliJ (pas Eclipse, dommage) peuvent directement [traduire du code Java en Kotlin](http://m.quickmeme.com/img/e4/e4e66bc437fb19844ca10d26f09dbe951a27b8e1da93b6054bb1f87a8ff06a62.jpg), mais y'en a qui ont essayé, ils ont eu des problèmes...

##### TODO :
 - [ ] Créer les mêmes classes que dans l'étape 1, mais en _Kotlin_ (donc `Meme` et `FavDir`).
 - [ ] Lui (`FavDIr`) ajouter un bloc _init_ qui print un truc dans la console (histoire de voir qu'il est bien initialisé une seule fois)
 - [ ] Lui rajouter la méthode `addMeme(meme: Meme)`, pour ajouter un meme
 - [ ] Et aussi `getAll()` qui (vous vous en doutez, bilingue que vous êtes) retourne à l'appelant la liste des memes du _FavDir_

## Étape 2 : Ajouter des memes a ses favoris

 Dans l'onglet de l'étape 2, vous trouverez un formulaire pour ajouter un meme dans votre _FavDir_. Pour la petite histoire, un dev avait déjà fait tout ça, mais il s'est fait virer... Fou de rage, il a pris cette implem, a supprimé deux trois trucs, et a _force push_, puis est parti... Bref, triste histoire...  
 Donc vous l'avez deviné : il va falloir remettre en place ce mécanisme, qui récupère la data du formulaire dans le back, et qui l'ajoute aux favoris. É qué sapelorio _Controller_. É _Service_, puisque faire des choses dans un controller (autres qu'appeler un service) c'est sale, et vous devriez avoir honte de le faire !

##### TODO :
  - [ ] Récupérer la data du formulaire dans un controller _Kotlin_ (vous gérez les noms, vous êtes grands).
  - [ ] Créer un service _Kotlin_ qui se chargera de l'ajout de votre meme dans la liste des favoris (et donc implicitement de votre fichier).

 **PRO TIPS :**
   - Il ne faut pas oublier que vous avez Spring derrière tout ça (pour votre plus grand BONHEUR). Et Spring a sa façon de faire.  
   Donc n'oubliez pas :
    - Les annotations @Service, @Controller, @Autowired, et j'en passe
    - Les modificateurs éventuels à ajouter aux cibles des annotations (open, lateinit...)

 Si tout se passe bien, ça marche (mais encore ?), et votre fichier est bien modifié en conséquence.


## Étape 3 : Qui a parlé de DB ?
Joli, **normalement** ça devait pas être trop long (surtout en _Kotlin_ hein), et vous devriez avoir un beau singleton.

Sinon nan pas une DB, c'est un peu overkill.  
Par contre, les ["DB"](https://i.imgflip.com/136hly.jpg), on peut faire. Par exemple... les écritures dans un fichier ? Il ne faudrait tout de même pas que vous perdiez votre liste de memes à chaque reboot de l'application.

Bon pour une fois, vous ne partez pas de rien. Vous avez dans le package _tools_ de _src/kotlin/...._ un fichier _FileManager_ qui vous servira de "DB". Par contre il n'y a pour l'instant qu'une méthode, pour savoir si une ligne non commentée est présente. Mais elle vous servira de base (ça ou [Google](www.bing.com) ofc).

##### TODO :
 - [ ] Agrémenter le _FileManager_ de deux méthodes, l'une pour charger le contenu du fichier dans votre _FavDir_, l'autre pour ajouter un meme au fichier.
 - [ ] Ajouter de la meilleure façon possible ([vous serez jugés](https://vignette.wikia.nocookie.net/aceattorney/images/8/87/Judge_Anime.png/revision/latest?cb=20160501045659)) un moyen pour que votre _FavDir_ se charge tout seul (sans bouton du front ni rien)
 - [ ] Faire (théoriquement, la pratique vient après) en sorte d'ajouter le meme au fichier quand vous l'ajoutez dans votre _FavDir_

## Étape 4 : Lister tout votre bordel
Bon c'est pas tout ça, mais il faut du visuel !  
Donc pour cette quatrième étape, vous devez afficher votre dossier de favoris dans l'onglet de l'étape 3 (classique). Par contre, étant donné qu'on ne lit pas votre code du futur, la jsp est un peu broken pour le moment, mais j'ai confiance en vous ! [Si Bob peut le réparer alors vous aussi !](https://www.youtube.com/watch?v=WfbZyaXDem0)

##### TODO :
  - [ ] Modifier la méthode du controller qui affiche les données dans l'onglet de l'étape 3 afin qu'elle affiche votre liste d'images favorites.
  - [ ] Faites vous "plaisir" et ajouter un tri sur la liste, ça va assez vite

## Étape 5 : Gotta go fast
Mmmmmh... c'est bizarre... ça met environ 40 ans à se charger...
Je pense qu'il est temps de faire un truc qu'on sait bien faire : se plaindre de l'infra.  
Ils ont mis en place la pire DB jamais vue dans l'histoire des pires DB. Elle. Est. Lente. Mais... très lente. Mais le client n'aime pas la lenteur, lui, il veut des trucs jolis et qui vont vite.  
Donc pas le choix... Il va falloir... parallèliser... _[\*queue drama music\*](https://www.youtube.com/watch?v=cphNpqKpKc4)_

Mais pas d'inquiétudes, c'est pas si compliqué que ça. On vous en a parlé vers la fin du diapo.  
Donc vous vous démerdez :-)

- [ ] Améliorez votre service qui get de façon à ce que vos images s'affichent en un temps raisonnable (environ 3 secondes, selon le nombre de memes que vous avez)

## Étape 6 : [Do what you want 'cause a pirate is free](https://www.youtube.com/watch?v=kRKAL-i-UbE)

Si vous arrivez ici, alors gg wp ! Normalement il devrait pas rester trop de temps à la séance, donc on a des propositions à vous faire, faites ce que vous voulez, notre travail ici est accompli (sauf s'il reste 1h30 auquel cas désolé M. Quinton de vous avoir fais faux pas)
 - [ ] Traduire le controller java en Kotlin
 - [ ] Faire des validateurs lors de la saisie d'url de meme (on peut faire des injections easy peasy atm (M. Cartigny aurait des envies de meurtre))
 - [ ] Faire des tris ou que sais-je avec les listes
 - [ ] Chercher d'autres memes sur l'internet
 - [ ] Cocher et décocher cette ligne 3 fois en moins de 1.5 secondes pour voir ce qu'il se passe
 - [ ] Se rendre compte que ça servait à rien
 - [ ] [try not to cry](https://i.imgur.com/AdiBPrO.jpg)
 - [ ] [cry a lot](https://i.imgur.com/AdiBPrO.jpg)
